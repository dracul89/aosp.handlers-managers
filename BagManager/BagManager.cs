﻿using System;
using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BagManager
{
    public class BagManager : AOPluginEntry
    {
        protected Settings _settings;

        public static Window _infoWindow;

        public static View _infoView;

        private static bool _init;

        public static string PluginDir;

        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private Queue<Item> toTrade = new Queue<Item>();

       
        public override void Run()
        {
            _settings = new Settings("BagManager");
            PluginDir = PluginDirectory;

            Game.OnUpdate += OnUpdate;
            Network.N3MessageSent += Network_N3MessageSent;

            _settings.AddVariable("Toggle", false);
            _settings["Toggle"] = false;

            Chat.RegisterCommand("manager", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["Toggle"] = !_settings["Toggle"].AsBool();
                Chat.WriteLine($"Bag : {_settings["Toggle"].AsBool()}");
            });

            Chat.RegisterCommand("delete", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Backpack bag = Inventory.Backpacks.Where(c => c.Name.Contains("Delete") && c.IsOpen && c.Items.Count > 0).FirstOrDefault();

                if (bag != null)
                {
                    foreach (Item item in bag?.Items)
                    {
                        item?.Delete();
                    }

                    Chat.WriteLine("Deleted.");
                }
            });

            _settings.AddVariable("ModeSelection", (int)ModeSelection.Swap);

            RegisterSettingsWindow("Bag Manager", "BagManagerSettingsView.xml");

            if (Game.IsNewEngine)
            {
                Chat.WriteLine("Does not work on this engine!");
            }
            else
            {
                Chat.WriteLine("BagManager Loaded!");
                Chat.WriteLine("/bagmanager for settings.");
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\BagManagerInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void HandleDeleteButtonClick(object s, ButtonBase button)
        {
            var bag = Inventory.Backpacks.FirstOrDefault(c => c.Name.Contains("Delete") && c.IsOpen && c.Items.Count > 0);

            if (bag?.Items == null) return;
            foreach (var item in bag.Items)
            {
                item.Delete();
            }

            Chat.WriteLine("Deleted.");
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            SwapItems(n3Msg);
        }

        private void SwapItems(N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity)
            {
                return;
            }

            if (!_settings["Toggle"].AsBool())
            {
                return;
            }

            if (ModeSelection.Swap != (ModeSelection)_settings["ModeSelection"].AsInt32())
            {
                return;
            }

            if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action == CharacterActionType.UseItemOnItem)
                {
                    var source = Inventory.Backpacks.FirstOrDefault(x => x.Slot.Instance == charActionMsg.Parameter2);
                    var target = Inventory.Backpacks.FirstOrDefault(x => x.Slot == charActionMsg.Target);

                    if (!(target?.Items.Count >= 1)) return;
                    foreach (var item in target.Items)
                    {
                        Network.Send(new ClientContainerAddItem()
                        {
                            Target = source.Identity,
                            Source = item.Slot
                        });
                    }
                }
            }
        }

        private void OnUpdate(object s, float deltaTime)
        {
            if (toTrade.Any())
            {
                toTrade.Peek().MoveToInventory();
                Item tradeItem = Inventory.Items.Where(c =>
                    c.Slot.Type == IdentityType.Inventory && c.Name == toTrade.Peek().Name &&
                    c.QualityLevel == toTrade.Peek().QualityLevel).FirstOrDefault();
                Task.Factory.StartNew(() =>
                {
                    TradeInventoryItem(tradeItem);
                },_cancellationTokenSource.Token);                 
            }
            else
            {
                AddTradeItems();
            }

            
            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                if (SettingsController.settingsWindow.FindView("BagManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (SettingsController.settingsWindow.FindView("DeleteButton", out Button deleteButton))
                {
                    deleteButton.Tag = SettingsController.settingsWindow;
                    deleteButton.Clicked = HandleDeleteButtonClick;
                }
                if (SettingsController.settingsWindow.FindView("SortButton", out Button sortButton))
                {
                    sortButton.Tag = SettingsController.settingsWindow;
                    sortButton.Clicked = HandleSortButtonClick;
                }
                if (SettingsController.settingsWindow.FindView("RenameSortButton", out Button renameSortButton))
                {
                    renameSortButton.Tag = SettingsController.settingsWindow;
                    renameSortButton.Clicked = HandleRenameSortButtonClick;
                }
                if (SettingsController.settingsWindow.FindView("RenameLootButton", out Button renameLootButton))
                {
                    renameLootButton.Tag = SettingsController.settingsWindow;
                    renameLootButton.Clicked = HandleRenameLootButtonClick;
                }
            }
        }

        private void HandleRenameLootButtonClick(object sender, ButtonBase e)
        {
            RenameSortBags();
        }

        private void HandleRenameSortButtonClick(object sender, ButtonBase e)
        {
            RenameLootBags();
        }

        private void RenameSortBags()
        {
            int num = 1;
            foreach (var backpack in Inventory.Backpacks.Where(b => b.Name.Contains("Sort")))
            {
                var name = "loot" + num;
                backpack?.SetName(name);
                num++;
            }
        }

        private void RenameLootBags()
        {
            foreach (var backpack in Inventory.Backpacks.Where(b => b.Name.Contains("loot")))
            {
                backpack?.SetName("Sort");
            }
        }


        private void HandleSortButtonClick(object sender, ButtonBase e)
        {
           SortBags();
        }

        private void AddTradeItems()
        {
            if (ModeSelection.Trade == (ModeSelection)_settings["ModeSelection"].AsInt32()
                && _settings["Toggle"].AsBool())
            {
                Backpack bag = Inventory.Backpacks.Where(c => c.Name.Contains("Trade") && c.IsOpen && c.Items.Count > 0).FirstOrDefault();
                if (bag == null)
                {
                    _init = false;
                    _settings["Toggle"] = false;
                    Chat.WriteLine("Finished.");
                    Chat.WriteLine("Toggle : False");
                    return;
                }

                _init = true;

                Chat.WriteLine("Started.");
                if (!toTrade.Any()) // if were have nothing in the trade queue then we need to add
                {
                    foreach (var item in bag.Items)
                    {
                        toTrade.Enqueue(item);
                        Chat.WriteLine("Added "+item.Name+" to trade queue.");
                    }
                    toTrade.Peek().MoveToInventory(); // change the first item in the queue
                }
            }
        }
        

        private void TradeInventoryItem(Item tradeItem)
        {
            Network.Send(new TradeMessage()
            {
                Unknown1 = 2,
                Action = TradeAction.AddItem,
                Param1 = (int)DynelManager.LocalPlayer.Identity.Type,
                Param2 = DynelManager.LocalPlayer.Identity.Instance,
                Param3 = (int)IdentityType.Inventory,
                Param4 = tradeItem.Slot.Instance
            });
            toTrade.Dequeue();
        }

        private void SortBags()
        {

            _init = true;

            List<Backpack> bags = Inventory.Backpacks.Where(c => c.Name.Contains("Sort") && c.Items.Count() < 21).ToList();

            Chat.WriteLine("Sorting.");
                
                
            Task.Factory.StartNew(() =>
            {
                foreach (var bag in bags)
                {
                    foreach (var item in bag.Items)
                    {
                        Backpack backpack = null;
                        foreach (var backpack1 in Inventory.Backpacks.Where(c => item.Name.Trim().Contains(c.Name.Trim()) && !"".Equals(c.Name)))
                        {
                            backpack = backpack1;
                        }
                                
                        if (backpack == null)
                        {
                            continue;
                        }
                        item.MoveToContainer(backpack);
                    }

                }

                _init = false;
                Chat.WriteLine("Sort Completed");

            }, _cancellationTokenSource.Token);

        }

        public static bool IsBackpack(Item item)
        {
            return item.Id == 275381 || item.Id == 143832 || item.Id == 157684 || item.Id == 157689 || item.Id == 157686 ||
                item.Id == 157691 || item.Id == 157692 || item.Id == 157693 || item.Id == 157683 || item.Id == 157682 ||
                item.Id == 287434 || item.Id == 157687 || item.Id == 157688 || item.Id == 157694 || item.Id == 157695 ||
                item.Id == 157690 || item.Id == 99241 || item.Id == 304586 || item.Id == 158790 || item.Id == 99228 ||
                item.Id == 223770 || item.Id == 152039 || item.Id == 156831 || item.Id == 259016 || item.Id == 259382 ||
                item.Id == 287419 || item.Id == 287420 || item.Id == 287421 ||
                item.Id == 287422 || item.Id == 287423 || item.Id == 287424 || item.Id == 287425 ||
                item.Id == 287427 || item.Id == 287429 || item.Id == 287430 ||
                item.Id == 287432 || item.Id == 287434 ||
                item.Id == 287440 || item.Id == 287441 ||
                item.Id == 287442 || item.Id == 287443 || item.Id == 287444 || item.Id == 287445 || item.Id == 287446 ||
                item.Id == 287447 || item.Id == 287448 || item.Id == 287610 ||
                item.Id == 287615 ||
                item.Id == 287617 || item.Id == 287619 || item.Id == 287620 || item.Id == 296977;
        }

        private enum ModeSelection
        {
            Swap, Trade
        }
        private enum NameSelection
        {
            Loot,Sort
        }
        
    }
}
